import 'bootstrap/dist/css/bootstrap.min.css';
import "./styles/styles.css";
import 'bootstrap';


import {modalArray} from "./js_modules/modal_Inners";
import {logInCheck, formCheck} from "./js_modules/login";
import {Visit} from "./js_modules/visit_class";


const btn = document.getElementById('createBtn')
const modal = document.getElementById('modalWindow')
//show modal depending on button innerHTML
 function showModal(button,modal,arr){
    if(button.innerHTML==='Log In'){
       modal.querySelector('.modal-content').innerHTML = arr[0]
        return button.innerHTML
    }
    else{
        modal.querySelector('.modal-content').innerHTML = arr[1]
        return button.innerHTML
    }
}

btn.addEventListener('click', async function (){
    await showModal(btn,modal,modalArray)
    if(btn.innerHTML ==='Log In'){
        const email = document.getElementById('email')
        const password = document.getElementById('password')
        await formCheck(modal, btn, logInCheck, email, password)
    } else{
        Visit.createSelect()
    }
})

