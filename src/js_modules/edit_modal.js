const token = "8c95741e-0c82-4db1-895f-32f72c80a629"
import{sendPutRequest} from "./send_put_request"
import { showCards } from "./show_cards"

export class EditModal {
    constructor(card){
    this.nameDoctor = card.nameDoctor 
    this.card = card
    this.sendEditRequest =this.sendEditRequest.bind(this)
}
show = ()=>{
    this.modal = document.createElement('div');
    this.modal.innerHTML = `<div id="myModal" class="modal">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <span class="close">&times;</span>
                                        <h2>Edit Patient Card</h2>
                                    </div>
                                     <div class="modal-body">
                                        <label>Patient First Name<input id="first-name-input" type="text" value =${this.card.firstName} ></input></label>
                                        <label>Patient Last Name<input id="last-name-input"type="text" value =${this.card.lastName} ></input></label>
                                        <label>Patient Middle Name<input id="middle-name-input" type="text" value =${this.card.middleName} ></input></label>
                                        <label>Purpose<input id="purpose-input" type="text" value =${this.card.purposeVisit} ></input></label>
                                        <label>Description<input id="description-input"type="text" value =${this.card.additionalComments} ></input></label>
                                        <label>Doctor<select id ="select-doctor">
                                        <option id="doctor-dentist">Dentist</option>
                                        <option id="doctor-cardiologist">Cardiologist</option>
                                        <option id="doctor-therapist">Therapist</option>
                                        </select></label>
                                        <label>Priority
                                        <select id ="select-priority">
                                        <option id = "option-low">Low</option>
                                        <option id = "option-medium">Medium</option>
                                        <option id = "option-high">High</option>
                                        </select></label>
                                        <label>Status<select id ="select-status">
                                        <option id="status-open">Open</option>
                                        <option id="status-close">Done</option>
                                        </select></label>
                                        <div class=other-inputs></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="edit-button">Edit</button>
                                    </div>
                                </div>
                            </div>`
                            
    document.querySelector(".card-manager").append(this.modal)
    this.selectDoctor(this.nameDoctor)
    this.selectPriority(this.card.priority)
    this.selectStatus(this.card.status)
    document.getElementById("myModal").style.display = "block";
    document.querySelector(".close").addEventListener("click", this.closeModal)
    let other;
    if(this.nameDoctor === "Dentist"){
        other = `  <label>Patient Last Invite<input id="last-invite-input" type="text" value =${this.card.lastVisit} ></input></label>`
    }
    if(this.nameDoctor === "Cardiologist"){
        other = ` <label>Patient Normal Pressure<input id = "normal-pressure-input" type="text" value =${this.card.normalPressure} ></input></label>
                        <label>Patient Weight Index<input id = "weight-index-input" type="text" value =${this.card.bodyMassIndex} ></input></label>
                        <label>Patient Previous Diseases<input id = "prev-diseases-input" type="text" value =${this.card.pastIllnesses} ></input></label>
                         <label>Patient Age<input id = "age-inputt" type="text" value =${this.card.age} ></input></label>`
    }
    if(this.nameDoctor === "Therapist"){
        other = `  <label>Patient Age<input id="age-input"type="text" value =${this.card.age} ></input></label>`
    }
    document.querySelector(".other-inputs").innerHTML = other
    document.getElementById("select-doctor").addEventListener("change", this.editOnChange)
    document.querySelector(".edit-button").addEventListener("click", this.sendEditRequest)
}
editOnChange = ()=>{
    let other;
    if(document.getElementById("select-doctor").value === "Dentist"){
        other = `  <label>Patient Last Invite<input id="last-invite-input" type="text" value =${this.card.lastVisit || ""} ></input></label>`
    }
    if(document.getElementById("select-doctor").value === "Therapist"){
        other = `  <label>Patient Age<input id="age-input"type="text" value =${this.card.age || ""} ></input></label>`
    }
    if(document.getElementById("select-doctor").value === "Cardiologist"){
        other = ` <label>Patient Normal Pressure<input id = "normal-pressure-input" type="text" value =${this.card.normalPressure || ""} ></input></label>
        <label>Patient Weight Index<input id = "weight-index-input" type="text" value =${this.card.bodyMassIndex || ""} ></input></label>
        <label>Patient Previous Diseases<input id = "prev-diseases-input" type="text" value =${this.card.pastIllnesses || ""} ></input></label>
         <label>Patient Age<input id = "age-input" type="text" value =${this.card.age || ""} ></input></label>`
    }
    document.querySelector(".other-inputs").innerHTML = other
}
selectDoctor = (doctor)=>{
    if(doctor === "Cardiologist"){
        document.getElementById("doctor-cardiologist").setAttribute("selected", "true");
    }
    if(doctor === "Therapist"){
        document.getElementById("doctor-therapist").setAttribute("selected", "true");
    }
    if(doctor === "Dentist"){
        document.getElementById("doctor-dentist").setAttribute("selected", "true");
    }
}
selectPriority = (priority) => {
    if(priority === "Low"){
        document.getElementById("option-low").setAttribute("selected", "true");
    }
    if(priority === "Medium"){
        document.getElementById("option-medium").setAttribute("selected", "true");
    }
    if(priority === "High"){
        document.getElementById("option-high").setAttribute("selected", "true");
    }
}

    selectStatus = (status) => {
        if(status === "Open"){
            document.getElementById("status-open").setAttribute("selected", "true");
        }
        if(status === "Done"){
            document.getElementById("status-close").setAttribute("selected", "true");
        }
    }
closeModal = ()=> {
    // event.preventDefault()
    this.modal.remove()
}
newValues = ()=>{
    this.card.firstName = document.getElementById("first-name-input").value
    this.card.lastName= document.getElementById("last-name-input").value
    this.card.middleName= document.getElementById("middle-name-input").value
    this.card.purposeVisit= document.getElementById("purpose-input").value
    this.card.additionalComments= document.getElementById("description-input").value
    this.card.priority= document.getElementById("select-priority").value
    this.card.status= document.getElementById("select-status").value
    this.card.nameDoctor= document.getElementById("select-doctor").value
    if(document.getElementById("last-invite-input")){this.card.lastVisit= document.getElementById("last-invite-input").value}
    if(document.getElementById("age-input")){this.card.age = document.getElementById("age-input").value}
    if(document.getElementById("normal-pressure-input")){this.card.normalPressure = document.getElementById("normal-pressure-input").value}
    if(document.getElementById("prev-diseases-input")){this.card.pastIllnesses = document.getElementById("prev-diseases-input").value}
    if(document.getElementById("weight-index-input")){this.card.bodyMassIndex = document.getElementById("weight-index-input").value}
}
async sendEditRequest () {
    this.newValues()
    await sendPutRequest(this.card);
    this.modal.remove()
    const filter = document.querySelector(".filter")
    if(filter){filter.remove();}
    showCards()
}
} 