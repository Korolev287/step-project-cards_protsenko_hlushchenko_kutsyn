import {createElement} from "./create_elem";
import {Window} from "./Window_class";
import {VisitTherapistCard, VisitCardiologistCard, VisitDentistCard} from "./visit_cards";

let globalObjectCards /*object to which new visit card properties will be assigned*/

//fill form with inputs, depended on doctor
function setInnerVisitForm(form, visitType = 'Doctor'){
    let innerForm = createElement('div', [],'innerForm')
    if (form.querySelector('#innerForm')){
        form.removeChild(form.querySelector('#innerForm'))
    }
    if(visitType === 'Doctor'){
        innerForm.innerHTML = ''
    } else {
        let visitInputs = visitType.createField()
        let stringHtml = document.createElement('div')
        stringHtml.appendChild( visitInputs.cloneNode(true))
        innerForm.innerHTML = stringHtml.innerHTML
    }
    form.appendChild(innerForm)
    return form
}

export class Visit {
    constructor(firstName, lastName, middleName, purposeVisit, priority,additionalComments, status = 'Open') {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.purposeVisit = purposeVisit;
        this.priority = priority
        this.additionalComments = additionalComments;
        this.status = status
    }
    //create visit form's fields
    static createField(){
        let fragment = document.createDocumentFragment();
        fragment.appendChild(createElement('input',['rounded-4', 'form-control'],'firstName','Enter your first name', true));
        fragment.appendChild(createElement('input',['rounded-4', 'form-control'],'lastName','Enter your last name', true));
        fragment.appendChild(createElement('input',['rounded-4', 'form-control'],'middleName','Enter your middle name', true));
        fragment.appendChild(createElement('input',['rounded-4', 'form-control'],'purposeVisit','Purpose of the visit', true));
        const priority = createElement('p', ['ms-2'], '', '',false, 'choose priority')
        const prioritySelect = createElement('select', ['ms-3'], 'prioritySelect')
        prioritySelect.appendChild(createElement('option', [], '', '', false, 'Low'))
        prioritySelect.appendChild(createElement('option', [], '', '', false, 'Medium'))
        prioritySelect.appendChild(createElement('option', [], '', '', false, 'High'))
        priority.appendChild(prioritySelect)
        fragment.appendChild(priority)
        fragment.appendChild(createElement('textarea', ['rounded-4','form-control'],'additionalComments','Enter comments'));
        return fragment
    }
    //get values from all fields of the visit form
    static getFieldValue(){
        let result={};
        result.firstName = document.getElementById('firstName').value;
        result.lastName = document.getElementById('lastName').value;
        result.middleName = document.getElementById('middleName').value;
        result.purposeVisit = document.getElementById('purposeVisit').value;
        const prioritySel = document.getElementById('prioritySelect')
        result.priority = prioritySel.options[prioritySel.selectedIndex].text
        result.additionalComments = document.getElementById('additionalComments').value;
        return result
    }
//create doctors select, show appropriate fields to each doctor and add event listeners: 'change' to this select and 'submit' to the form event
    static createSelect(){
        let form = document.getElementById('VisitForm')
        console.log(form)
        const dialogSelect = createElement('select',["form-select"])
        let dialogSelectDefault = createElement('option',[],"","",false,"Doctor")
        dialogSelect.appendChild(dialogSelectDefault)

        const dialogSelectOpt = createElement('option',[],"","",false,"Cardiologist")
        dialogSelectOpt.value='Cardiologist'
        dialogSelect.appendChild(dialogSelectOpt.cloneNode(true))
        dialogSelectOpt.value='Dentist'
        dialogSelectOpt.innerText = 'Dentist'
        dialogSelect.appendChild(dialogSelectOpt.cloneNode(true))
        dialogSelectOpt.value='Therapist'
        dialogSelectOpt.innerText = 'Therapist';
        dialogSelect.appendChild(dialogSelectOpt.cloneNode(true))

        dialogSelect.addEventListener('change',function () {
            if(this.value==="Therapist"){
                setInnerVisitForm(form, TherapistVisit)
            }else if(this.value==="Cardiologist"){
                setInnerVisitForm(form, CardiologistVisit)
            }else if(this.value==='Dentist') {
                setInnerVisitForm(form, DentistVisit)
            }else if(this.value ==='Doctor'){
                setInnerVisitForm(form)
            }
            form.addEventListener('submit',function (event) {
                let selected =  dialogSelect.options[dialogSelect.selectedIndex].value;
                let foundInform;
                let visitCard;
                if(selected==="Therapist"){
                    foundInform = TherapistVisit.getFieldValue();
                    globalObjectCards = new TherapistVisit(foundInform['purposeVisit'],foundInform['age'],foundInform['firstName'], foundInform['lastName'], foundInform['middleName'], foundInform['priority'], foundInform['additionalComments']);
                    visitCard = VisitTherapistCard
                }else if(selected==="Cardiologist") {
                    foundInform = CardiologistVisit.getFieldValue();
                    globalObjectCards = new CardiologistVisit(foundInform['purposeVisit'],foundInform['normalPressure'],foundInform['bodyMassIndex'], foundInform['pastIllnesses'],foundInform['age'], foundInform['firstName'], foundInform['lastName'], foundInform['middleName'],foundInform['priority'], foundInform['additionalComments'])
                    visitCard = VisitCardiologistCard
                }else if(selected==='Dentist'){
                    foundInform = DentistVisit.getFieldValue();
                    globalObjectCards = new DentistVisit(foundInform['purposeVisit'],foundInform['lastVisit'],foundInform['firstName'], foundInform['lastName'], foundInform['middleName'],foundInform['priority'], foundInform['additionalComments'])
                    visitCard = VisitDentistCard
                }else {
                    event.preventDefault();
                }
                event.preventDefault()
                Window.modalHide(document.getElementById('modalWindow'))

                fetch("https://ajax.test-danit.com/api/v2/cards", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify(globalObjectCards)
                })
                    .then(response => response.json())
                    .then(response => {const newVisitCard = new visitCard(response)
                                        newVisitCard.render()})

            });
        });

        form.appendChild(dialogSelect);
        return form;
    }

}

class CardiologistVisit extends Visit{
    constructor(purposeVisit,normalPressure,bodyMassIndex, pastIllnesses,age, firstName, lastName, middleName, priority, additionalComments, status){
        super(firstName, lastName, middleName, purposeVisit, priority, additionalComments, status);
        this.nameDoctor = "Cardiologist";
        this.age=age;
        this.normalPressure = normalPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.pastIllnesses = pastIllnesses;

    }

    static createField() {
        let fragment = super.createField();
        let age =createElement('input',['rounded-4', 'form-control'],'age','Enter your age', true);
        age.setAttribute('type','number');
        fragment.appendChild(age);
        fragment.appendChild(createElement('input',['rounded-4', 'form-control'],'normalPressure','Enter your normal pressure', true));
        fragment.appendChild(createElement('input',['rounded-4','form-control'],'bodyMassIndex','Enter your body mass index', true));
        fragment.appendChild(createElement('input',['rounded-4','form-control'],'pastIllnesses','Enter your past diseases of the cardiovascular system', true));
        return fragment;
    }

    static getFieldValue() {
        let result = super.getFieldValue();
        result.age=document.getElementById('age').value;
        result.normalPressure=document.getElementById('normalPressure').value;
        result.bodyMassIndex=document.getElementById('bodyMassIndex').value;
        result.pastIllnesses=document.getElementById('pastIllnesses').value;
        return result
    }
}

class DentistVisit extends Visit{
    constructor(purposeVisit,lastVisit,firstName, lastName, middleName,priority, additionalComments, status){
        super(firstName, lastName, middleName, purposeVisit,priority, additionalComments, status);
        this.lastVisit = lastVisit;
        this.nameDoctor = "Dentist";
    }

    static createField() {
        let fragment = super.createField();
        fragment.appendChild(createElement('input',['rounded-4','form-control'],'lastVisit','Enter date of last visit', true));
        return fragment;
    }

    static getFieldValue() {
        let result = super.getFieldValue();
        result.lastVisit = document.getElementById("lastVisit").value;
        return result;
    }
}

class TherapistVisit extends Visit {
    constructor(purposeVisit,age,firstName, lastName, middleName, priority, additionalComments, status){
        super(firstName, lastName, middleName, purposeVisit, priority, additionalComments, status);
        this.age=age;
        this.nameDoctor = "Therapist";

    }

    static createField(){
        let fragment = super.createField();
        let age =createElement('input',['rounded-4', 'form-control'],'age','Enter your age', true);
        age.setAttribute('type','number');
        fragment.appendChild(age);
        return fragment;
    }

    static getFieldValue() {
        let result = super.getFieldValue();
        result.age = document.getElementById('age').value;
        return result
    }

}