import {Window} from "./Window_class";
// import {showCardManager,showCards,} from "./show_cards";
import { showCardManager, showCards } from "./show_cards";



//validate email and password
function logInCheck(email, password) {
    return email.value !== 'm@gm.com' || password.value !== '123';
}

//doesn't allow to submit the form with incorrect data and change button after login
function formCheck(modal, btn, logInCheck, email, password){
    let loginForm = document.getElementById('loginForm')
    loginForm.addEventListener('submit', event => {
        const inputs = modal.querySelectorAll('.form-control')
        if (logInCheck(email, password)) {
            event.preventDefault();
            event.stopPropagation();
            inputs.forEach(input => {
                input.classList.add('btn-outline-danger')
            })
        } else {
            event.preventDefault();
            fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: `${email.value}`, password: `${password.value}` })
            })
                .then(response => response.text())
                .then(data => {
                    btn.innerHTML = 'Create'
                    localStorage.setItem("token", data);
                })
                // .then(data => {
                //     token = data
                //     btn.innerHTML = 'Create'
                // })
            // return token
            Window.modalHide(modal)
            showCardManager()
            showCards()
        }
    })
}

export {logInCheck, formCheck}

