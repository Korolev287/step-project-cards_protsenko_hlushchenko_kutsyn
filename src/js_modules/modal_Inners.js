let logInModalHTML = `
        <div class="modal-header p-5 pb-4 border-bottom-0">
        <h2 class="fw-bold mb-0">Log In</h2>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="close"></button>
      </div>
      <div class="modal-body p-5 pt-0">
        <form class="" id="loginForm">
          <div class="form-floating mb-3">
            <input type="email" class="form-control rounded-4" id="email" placeholder="name@example.com" required="required">
            <label for="email">Email address</label>
          </div>
          <div class="form-floating mb-3">
            <input type="password" class="form-control rounded-4 " id="password" placeholder="Password" required="required">
            <label for="password">Password</label>
          </div>
          <button class="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit">Log In</button>
        </form>
        </div>
        `
let creationModal = `
    <div class="modal-header p-5 pb-4 border-bottom-0">
        <h2 class="fw-bold mb-0"></h2>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="close"></button>
      </div>
      <div class="modal-body p-5 pt-0">
        <form class="" id="VisitForm">
          <button class="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit">Create Visit</button>
        </form>
        </div>
        `

let modalArray = [logInModalHTML, creationModal]
export {modalArray}