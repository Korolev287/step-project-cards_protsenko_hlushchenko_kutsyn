const token = "8c95741e-0c82-4db1-895f-32f72c80a629"
export async function sendPutRequest(card){
    let requestBody 
    if(card.nameDoctor === "Dentist"){
        requestBody = JSON.stringify({
            additionalComments: card.additionalComments,
            firstName: card.firstName,
            id: card.id,
            lastName: card.lastName,
            middleName: card.middleName,
            nameDoctor: "Dentist",
            priority: card.priority,
            purposeVisit: card.purposeVisit,
            status: card.status,
            lastVisit: card.lastVisit,
    })
    }
    if(card.nameDoctor === "Therapist"){
        requestBody =  JSON.stringify({
            additionalComments: card.additionalComments,
            firstName: card.firstName,
            id: card.id,
            lastName: card.lastName,
            middleName: card.middleName,
            nameDoctor: "Therapist",
            priority: card.priority,
            purposeVisit: card.purposeVisit,
            status: card.status,
            age: card.age,

        })
    }
    if(card.nameDoctor === "Cardiologist"){
        requestBody = JSON.stringify({
            additionalComments: card.additionalComments,
            age: card.age,
            bodyMassIndex: card.bodyMassIndex,
            firstName: card.firstName,
            id: card.id,
            lastName: card.lastName,
            middleName: card.middleName,
            nameDoctor: "Cardiologist",
            normalPressure: card.normalPressure,
            pastIllnesses: card.pastIllnesses,
            priority: card.priority,
            purposeVisit: card.purposeVisit,
            status: card.status,
        })
    }
       const responce = await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
        },
        body: requestBody
        })
        const putRequest = await responce.json()
}