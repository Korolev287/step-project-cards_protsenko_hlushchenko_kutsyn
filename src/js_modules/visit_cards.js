import {EditModal} from "./edit_modal";
class Element{
    createElement(elementName, className = [], text){
        const element = document.createElement(elementName);
        element.classList.add(...className)
        if (text) { element.innerHTML = text; }
        return element;
    }
}
class VisitCard extends Element{
    constructor(card){
        super()
        this.patientFirstName = card.firstName;
        this.patientLastName = card.lastName;
        this.patientMiddleName = card.middleName;
        this.patientName = ()=> {return `${this.patientFirstName} ${this.patientLastName} ${this.patientMiddleName} `}
        this.doctorName = card.nameDoctor
        this.purpose = card.purposeVisit
        this.description = card.additionalComments
        this.priority = card.priority
        this.showMore = this.showMore.bind(this)
        this.edit = this.edit.bind(this)
        this.removeCard = this.removeCard.bind(this)
        this.id = card.id
        this.el = card
        this.status = card.status
    }
    render(){
        this.card = this.createElement('div', ["visit-card"]);
        const patientNameEl = this.createElement("div", ["card-item"], `<span class="card-item-name">Patient:</span><span class="card-item-value">${this.patientName()}</span>`);
        const doctorNameEl = this.createElement("div", ["card-item"], `<span class="card-item-name">Doctor:</span><span class="card-item-value"> ${this.doctorName}</span>`);
        this.purposeEl = this.createElement("div", ["card-item", "hidden"], `<span class="card-item-name">Purpose:</span><span class="card-item-value"> ${this.purpose}</span>`);
        this.descriptionEl = this.createElement("div", ["card-item", "hidden"], `<span class="card-item-name">Description:</span><span class="card-item-value"> ${this.description}</span>`)
        this.priorityEl = this.createElement("div", ["card-item", "hidden"], `<span class="card-item-name">Priority:</span><span class="card-item-value"> ${this.priority}</span>`)
        this.statusEl = this.createElement("div", ["card-item", "hidden"], `<span class="card-item-name">Status:</span><span class="card-item-value"> ${this.status}</span>`)

        this.showLessBtn = this.createElement("button", ["show-less-btn", "hidden"], "Show Less...")
        this.showLessBtn.addEventListener("click", this.showMore)

        this.showMoreBtn = this.createElement("button", ["show-more-btn"], "Show More...")
        this.showMoreBtn.addEventListener("click", this.showMore)

        const deleteBtn = this.createElement("button", ["delete-btn"], "X")
        deleteBtn.addEventListener("click", this.removeCard)

        const editBtn = this.createElement("button", ["edit-btn"], "Edit")
        editBtn.addEventListener("click", this.edit)

        this.card.append(editBtn, deleteBtn, patientNameEl, doctorNameEl,this.showMoreBtn,this.showLessBtn, this.purposeEl, this.descriptionEl, this.priorityEl, this.statusEl)
        
        const title = document.querySelector(".card-manager-title")
        if(title){title.remove()}
        document.querySelector(".card-manager").append(this.card)
        // return this.card
    }
    showMore(){
        this.purposeEl.classList.toggle('hidden')
        this.descriptionEl.classList.toggle('hidden')
        this.priorityEl.classList.toggle('hidden')
        this.statusEl.classList.toggle('hidden')
        this.showMoreBtn.classList.toggle('hidden')
        this.showLessBtn.classList.toggle('hidden')
    }
    removeCard(event){
        event.target.closest('.visit-card').remove()
        fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
            method: 'DELETE',
            headers: {
        'Authorization': `Bearer 8c95741e-0c82-4db1-895f-32f72c80a629`
            },
        })
    }
    edit(){
        const editModal = new EditModal(this.el)
        editModal.show()
    }
}

export class VisitDentistCard extends VisitCard {
    constructor(card) {
        super(card)
        this.patientLastInvite = card.lastVisit
    }
    render(){
        super.render()
        this.lastInviteEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Last Invite:</span><span class="card-item-value">${this.patientLastInvite}</span>`)
        this.card.append(this.lastInviteEl)
    }
    showMore(){
        super.showMore()
        this.lastInviteEl.classList.toggle('hidden')
    }
}

export class VisitCardiologistCard extends VisitCard {
    constructor(card) {
        super(card)
        this.normalPressure = card.normalPressure
        this.weightIndex = card.bodyMassIndex
        this.previousDiseases = card.pastIllnesses
        this.age = card.age
    }
    render(){
        super.render()
        this.normalPressureEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Normal Pressure: </span><span class="card-item-value">${this.normalPressure}</span>`)
        this.weightIndexEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Weight Index:</span><span class="card-item-value"> ${this.weightIndex}</span>`)
        this.previousDiseasesEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Previous Diseases:</span><span class="card-item-value"> ${this.previousDiseases}</span>`)
        this.ageEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Age: </span><span class="card-item-value">${this.age}</span>`)
        this.card.append(this.normalPressureEl, this.weightIndexEl, this.previousDiseasesEl, this.ageEl)
    }
    showMore(){
        super.showMore()
        this.normalPressureEl.classList.toggle('hidden')
        this.weightIndexEl.classList.toggle('hidden')
        this.previousDiseasesEl.classList.toggle('hidden')
        this.ageEl.classList.toggle('hidden')
    }
}

export class VisitTherapistCard extends VisitCard {
    constructor(card) {
        super(card)
        this.age = card.age
    }
    render(){
        super.render()
        this.ageEl = this.createElement("p", ["card-item", "hidden"], `<span class="card-item-name">Age: </span><span class="card-item-value">${this.age}</span>`)
        this.card.append(this.ageEl)
    }
    showMore(){
        super.showMore()
        this.ageEl.classList.toggle('hidden')
    }
}