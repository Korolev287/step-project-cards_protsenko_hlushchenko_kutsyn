import { VisitCardiologistCard } from './visit_cards';
import { VisitDentistCard } from './visit_cards';
import { VisitTherapistCard } from './visit_cards';


export async function showCards() {
  document.querySelector(".card-manager").innerHTML = ''
  const cards = await fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then(response => response.json())

  function createSerchForm() {
    const divFilter = document.createElement('div');
    divFilter.classList.add('filter')
    const navBar = document.querySelector('.navbar')
    const form = document.createElement('form');
    const input = document.createElement('input');
    const firstSelect = document.createElement('select');
    const done = document.createElement('option');
    done.innerText = 'Done';
    const open = document.createElement('option');
    open.innerText = 'Open';
    const secondSelect = document.createElement('select');
    const high = document.createElement('option');
    high.innerText = 'High';
    const normal = document.createElement('option');
    normal.innerText = 'Medium';
    const low = document.createElement('option');
    low.innerText = 'Low';
    const serchButton = document.createElement('button');
    serchButton.setAttribute('type', 'submit');
    serchButton.innerText = 'Search';
    secondSelect.append(high, normal, low)
    firstSelect.append(done, open)

    form.addEventListener('submit', (event) => {
      event.preventDefault();
      const visitCards = document.querySelectorAll('.visit-card')
      visitCards.forEach(item => item.remove())
      const input = event.target.elements[0].value;
      const firstSelect = event.target.elements[1].value;
      const secondSelect = event.target.elements[2].value;
      const filterArr = cards.filter((el) => {
        return (el.additionalComments.includes(input) || el.purposeVisit.includes(input))
          && el.status === firstSelect
          && el.priority === secondSelect;
      })

      filterArr.forEach(card => {
        if (card.nameDoctor === "Cardiologist") {
          new VisitCardiologistCard(card).render()
        }
        if (card.nameDoctor === "Dentist") {
          new VisitDentistCard(card).render()
        }
        if (card.nameDoctor === "Therapist") {
          new VisitTherapistCard(card).render()
        }
      }
      );
    })

    form.append(input, firstSelect, secondSelect, serchButton)
    navBar.after(divFilter)
    divFilter.appendChild(form)
  }

  createSerchForm()

  if (cards.length === 0) {
    const title = document.createElement('h2');
    title.innerText = "No items have been added"
    title.classList.add('card-manager-title');
    document.querySelector(".card-manager").append(title);
  }

  cards.forEach(card => {
    if (card.nameDoctor === "Cardiologist") {
      new VisitCardiologistCard(card).render()
    }
    if (card.nameDoctor === "Dentist") {
      new VisitDentistCard(card).render()
    }
    if (card.nameDoctor === "Therapist") {
      new VisitTherapistCard(card).render()
    }
  }
  );
}
export function showCardManager() {
  const cardManager = document.createElement('div')
  cardManager.classList.add('card-manager')
  document.body.append(cardManager)
  
}

