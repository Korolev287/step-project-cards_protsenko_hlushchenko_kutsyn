export class Window {
    static modalHide(modal){
        modal.style.display = 'none'
        document.querySelector('body').classList.remove('modal-open')
        let modalBack = document.querySelector('.modal-backdrop')
        modalBack.remove()
    }
}